{# Corps lettre réponse : personne connue, récupération possible #}


<p>Maître,</p>

<p>Vous nous informez que vous êtes chargé du règlement de la succession de {{sexe}} {{prenom}} {{nom}} {{naissance}} 
le {{date_naissance}}.</p>

<p>{{sexe}} {{prenom}} {{nom}} a bénéficié de prestations d'aide sociale, qui est une aide récupérable. 

Le Département est, de ce fait, créancier de sa succession. Il est en mesure de recouvrer sur la succession du 
bénéficiaire les sommes avancées, en application de l'article L.132-8 du code de l'action sociale et des familles.</p>

<p>Dans le cadre de l'instruction d'un éventuel recours en récupération, je vous remercie de me faire parvenir, 
dans les meilleurs délais, un détail actif/passif de ladite succession, à défaut de la copie de la déclaration, ainsi 
que la copie des documents relatifs aux contrats d'assurance vie contractés le cas échéant par le défunt.</p>

<p>Je vous prie de me communiquer les éléments concernant le règlement des frais d'obsèques : en me transmettant la 
facture acquittée, et m'indiquant si ces frais ont été prélevés sur le compte du défunt ou avancés par un tiers. 
En effet, les frais funéraires peuvent être prélevés sur le compte du défunt dans la limite de 3 500 euros.</p> 

<p>Je vous remercie enfin de m'indiquer si l'établissement qui hébergeait {{sexe}} {{prenom}} {{nom}} détient entre ses 
mains des fonds déposés.</p>

<p> Je vous prie d'agréer, Maître, mes courtoises salutations.</p>
