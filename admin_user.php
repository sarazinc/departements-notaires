<?php
$search='';
if (getPost('enregistrer') == 'Enregistrer') {
	$login = getPost('login');
	$description= getPost('description');
	$mail_login= getPost('mail');
	$profil= getPost('profil');
	
	$search = [
			"login"  				 => $login,
			"description"    => $description,
			"mail"           => $mail_login,
			"profil"         => $profil,
	];
	
	
	if (! empty($login) && ! empty($description) && ! empty($mail_login) && ! empty($profil)) {
		// on teste si une entrée de la base contient ce login
		$sql = "SELECT count(*) as nb FROM membre WHERE login=". sql_escape($login);
		$result = $connect->query($sql);
		$data = $result->fetch();
		
		if($data["nb"] == 1){
			$message = "Ce login existe déjà !";
		}else{
			if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/",$mail_login)){
				$message = "L'adresse mail n'est pas conforme !";
			}else{
				$sql = "SELECT count(*) as nb FROM membre WHERE adresse_mail=". sql_escape($mail_login);
				$result = $connect->query($sql);
				$data1 = $result->fetch();
				if($data1["nb"] == 1){
					$message = "Ce mail existe déjà !";
				}else{
					$mtp=generer_mot_de_passe();
					
					$sql = "INSERT INTO membre(id, login, libelle, pass_md5, profile, adresse_mail) VALUES (NULL,".sql_escape($login).",".sql_escape($description).",
					".sql_escape(_passCrypt($mtp)).",".sql_escape($profil).",".sql_escape($mail_login).")";
					$result = $connect->query($sql);
					
					$search = [
							"login"  				 => $login,
							"description"    => $description,
							"mail"           => $mail_login,
							"profil"         => $profil,
							"motdepasse"		 => $mtp,
					];
					
					
					// Sujet commun à plusieurs messages
					$mail_sujet = sprintf("[%s] Création du compte pour %s",
							config('nom_application'),
							$search['description']);
					
				// TODO externaliser dans TPL
					$mail_corps = <<<_CORPS_MAIL_
	        <style>
	          .signature{font-family:'verdana';font-size:75%%;font-weight:bold;}
	          .corps{font-family:'verdana';font-size:80%%;}
	          li{font-family:'verdana';font-size:80%%;}
	        </style>
	        <p class=corps><br/>Bonjour,<br/><br/>Voici les informations nécessaires à votre étude, pour la connexion à
					l'application %s.<br/>
	        </p>
	        <p class=corps>•	L'adresse internet de connexion est %s<br/>
					•	L'identifiant de votre étude correspond à votre code CRPCEN est : %s<br/>
					•	Votre mot de passe est : %s<br/>
					</p>
					<p class=corps>
					<u>Attention : Il vous sera demandé de personnaliser le mot de passe de votre étude dès votre première connexion.</u><br/>
					<br/>
					Un document d'aide à l'utilisation est présent sur la page d'accueil de l'application.<br/>
					<b>Pour toute anomalie, merci de contacter le bureau suivi des dispositifs du %s (%s).</b><br/>
					</p>
	        <p class=corps>Cordialement,</p><br/>
	        <p class=signature>L'équipe %s<br/>%s</p>
_CORPS_MAIL_;
					$mail_corps = sprintf($mail_corps, config('nom_application'), config('adresse_url_appli'), $search['login'], $search['motdepasse'], 
						config('nom_departement_long'), config('mail_gestion'),config('nom_application'),config('nom_departement_long'));

					sendResponseEmail($mail_login, null, $mail_sujet, $mail_corps, null);
					$message = "Le nouveau membre de l'application a été bien créé.<br/><a class='reinit'  href='index.php'>Retour à l'accueil</a>";
				}
			}
		}
	}
	else{
		$message = "Au moins un des champs est vide";
	}
}

/**
 * Génération d'un mot de passe aléatoirement.
 *
 * @param int $nb_caractere nouveau mot de passe
 * @return string $mot_de_passe
 * 
 */
function generer_mot_de_passe($nb_caractere = 12)
{
	$mot_de_passe = "";
	
	$chaine = "aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWXxYyZz0123456789+_-#@!$%?&";
	$longeur_chaine = strlen($chaine);
	
	for($i = 1; $i <= $nb_caractere; $i++)
	{
		$place_aleatoire = mt_rand(0,($longeur_chaine-1));
		$mot_de_passe .= $chaine[$place_aleatoire];
	}
	
	return $mot_de_passe;
}



echo templateRender("sys/frm_create_user.tpl", $search);
?>
<br />

<div id="message_r">
    <?php
    if (isset($message)) {
        echo $message;
    }
    ?>
</div>